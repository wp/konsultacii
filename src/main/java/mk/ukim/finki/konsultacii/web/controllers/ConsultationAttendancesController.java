package mk.ukim.finki.konsultacii.web.controllers;

import lombok.AllArgsConstructor;
import mk.ukim.finki.konsultacii.model.ConsultationAttendance;
import mk.ukim.finki.konsultacii.service.ConsultationAttendanceService;
import mk.ukim.finki.konsultacii.service.ProfessorService;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import java.util.List;

@Controller
@AllArgsConstructor
public class ConsultationAttendancesController {

    private final ConsultationAttendanceService consultationAttendanceService;
    private final ProfessorService professorService;

    @GetMapping(value="/studentAttendances", params = "!time")
    public String getStudentAttendances(RedirectAttributesModelMap redirectModel) {
        redirectModel.put("time", "UPCOMING");
        return "redirect:/studentAttendances";
    }

    @GetMapping(value = "/studentAttendances", params = "time")
    public String getStudentAttendances(Model model,
                                        @RequestParam(required = false) String type,
                                        @RequestParam(required = true) String time,
                                        @RequestParam(defaultValue = "1") Integer pageNum,
                                        @RequestParam(defaultValue = "10") Integer results) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String studentUsername = authentication.getName();

        Page<ConsultationAttendance> attendancesPage = consultationAttendanceService.
                listAttendancesPage(time, type, "", studentUsername, results, pageNum);

        model.addAttribute("page", attendancesPage);
        model.addAttribute("username", studentUsername);
        return "bookedConsultations/studentBookedConsultations";
    }

    @GetMapping(value="/professorAttendances", params = "!time")
    public String getProfessorAttendances(RedirectAttributesModelMap redirectModel) {
        redirectModel.put("time", "UPCOMING");
        return "redirect:/professorAttendances";
    }

    @GetMapping(value = "/professorAttendances", params = "time")
    public String getProfessorAttendances(Model model,
                                          @RequestParam(required = false) String type,
                                          @RequestParam(required = true) String time,
                                          @RequestParam(required = false) String studentIndex,
                                          @RequestParam(defaultValue = "1") Integer pageNum,
                                          @RequestParam(defaultValue = "10") Integer results) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String professorUsername = authentication.getName();

        Page<ConsultationAttendance> attendancePage = consultationAttendanceService.listAttendancesPage(time, type,
                professorUsername, studentIndex, results, pageNum);
        model.addAttribute("page", attendancePage);
        model.addAttribute("professorUsername", professorUsername);
        model.addAttribute("username", professorUsername);
        return "bookedConsultations/professorBookedConsultations";
    }

    @GetMapping(value="/adminAttendances", params = "!time")
    public String getAdminAttendances(RedirectAttributesModelMap redirectModel) {
        redirectModel.put("time", "UPCOMING");
        return "redirect:/adminAttendances";
    }

    @GetMapping(value = "/adminAttendances", params = "time")
    public String getAdminAttendances(Model model,
                                      @RequestParam(required = false) String type,
                                      @RequestParam(required = true) String time,
                                      @RequestParam(required = false) String professorId,
                                      @RequestParam(required = false) String studentIndex,
                                      @RequestParam(defaultValue = "1") Integer pageNum,
                                      @RequestParam(defaultValue = "10") Integer results) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Page<ConsultationAttendance> attendancePage = consultationAttendanceService.listAttendancesPage(time, type,
                professorId, studentIndex, results, pageNum);

        model.addAttribute("username", username);
        model.addAttribute("page", attendancePage);
        model.addAttribute("professors", professorService.listAllProfessors(""));
        return "bookedConsultations/adminBookedConsultations";
    }

    @PostMapping("/studentAttendances/reportAbsentProfessor")
    public String reportAbsentProfessor(@RequestParam Long attendanceId,
                                        @RequestParam(required = false) Boolean reportAbsentProfessor,
                                        @RequestParam(required = false) String absentProfessorComment) {
        consultationAttendanceService.reportAbsentProfessor(attendanceId, reportAbsentProfessor, absentProfessorComment);
        return "redirect:/studentAttendances";
    }

    @PostMapping("/professorAttendances/reportAbsentStudent")
    public String reportAbsentStudent(@RequestParam Long attendanceId,
                                      @RequestParam(required = false) Boolean reportAbsentStudent,
                                      @RequestParam(required = false) String absentStudentComment) {
        consultationAttendanceService.reportAbsentStudent(attendanceId, reportAbsentStudent, absentStudentComment);
        return "redirect:/professorAttendances";
    }

    @PostMapping("/studentAttendances/cancel")
    public String cancelAttendance(@RequestParam Long attendanceId,
                                   RedirectAttributes redirectAttributes) {
        try {
            consultationAttendanceService.deleteConsultationAttendanceById(attendanceId);
            redirectAttributes.addFlashAttribute("successMessage", "Успешно го откажавте присуството.");
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("errorMessage", "Грешка при откажувањето на консултацијата.");
        }

        return "redirect:/studentAttendances";
    }
}
