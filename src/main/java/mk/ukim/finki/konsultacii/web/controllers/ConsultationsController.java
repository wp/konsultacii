package mk.ukim.finki.konsultacii.web.controllers;

import lombok.AllArgsConstructor;
import mk.ukim.finki.konsultacii.config.FacultyUserDetails;
import mk.ukim.finki.konsultacii.model.Consultation;
import mk.ukim.finki.konsultacii.model.ConsultationAttendance;
import mk.ukim.finki.konsultacii.model.Professor;
import mk.ukim.finki.konsultacii.model.Student;
import mk.ukim.finki.konsultacii.model.enumerations.UserRole;
import mk.ukim.finki.konsultacii.service.*;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import java.time.LocalDateTime;
import java.util.*;


@Controller
@RequestMapping("/consultations")
@AllArgsConstructor
public class ConsultationsController {

    private ConsultationAttendanceService consultationAttendanceService;

    private ProfessorService professorService;

    private ConsultationService consultationService;

    @GetMapping(params = "!time")
    public String getConsultationPage(Model model,
                                      @RequestParam(required = false) String type,
                                      @RequestParam(required = false) String professor,
                                      RedirectAttributesModelMap redirectModel
    ) {
        redirectModel.put("type", type);
        redirectModel.put("professor", professor);
        redirectModel.put("time", "UPCOMING");
        return "redirect:/consultations";
    }

    @GetMapping(params = "time")
    public String getConsultationsPage(Model model,
                                       @RequestParam(required = false) String type,
                                       @RequestParam(required = false) String professor,
                                       @RequestParam(required = true) String time,
                                       @RequestParam(defaultValue = "1") Integer pageNum,
                                       @RequestParam(defaultValue = "5") Integer results) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        TreeMap<Character, List<Professor>> professors = this.professorService.findAllProfessorsSortedByFirstName(professor);

        String studentUsername = authentication.getName();

        Page<ConsultationAttendance> attendances = Page.empty();
        if(authentication.getAuthorities().stream().anyMatch(auth -> auth.getAuthority().equals("ROLE_STUDENT"))){
            attendances = consultationAttendanceService
                    .listAttendancesPage(time, type, "", studentUsername, results, pageNum);
        }
        boolean isAuthenticated = !authentication.getPrincipal().equals("anonymousUser");
        String username = "";
        if(isAuthenticated){
            username = authentication.getName();
        }
        model.addAttribute("isAuthenticated", isAuthenticated);
        model.addAttribute("username", username);
        model.addAttribute("page", attendances);
        model.addAttribute("professorsMap", professors);
        return "index";
    }

    @GetMapping(value = "/professor/{id}")
    public String getConsultationsByProfessor(@PathVariable("id") String professorId,
                                              @RequestParam(defaultValue = "1") Integer regularPageNum,
                                              @RequestParam(defaultValue = "5") Integer regularResults,
                                              @RequestParam(defaultValue = "1") Integer irregularPageNum,
                                              @RequestParam(defaultValue = "5") Integer irregularResults,
                                              Model model) {
        String studentEmail = getStudentEmail();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        boolean isAuthenticated = !authentication.getPrincipal().equals("anonymousUser");
        Professor professor = this.professorService.getProfessorById(professorId);
        model.addAttribute("professor", professor);
        model.addAttribute("username", authentication.getName());

        if(authentication.getAuthorities().stream().anyMatch(auth -> auth.getAuthority().equals("ROLE_PROFESSOR"))
                || authentication.getAuthorities().stream().anyMatch(auth -> auth.getAuthority().equals("ROLE_ADMIN"))){
            List<Consultation> regularConsultationTerms = this.consultationService
                    .listConsultationTermsByProfessor(professorId, "WEEKLY","UPCOMING","ACTIVE");
            List<Consultation> irregularConsultationTerms = this.consultationService
                    .listConsultationTermsByProfessor(professorId, "ONE_TIME","UPCOMING", "ACTIVE");
            model.addAttribute("irregularConsultationTerms", irregularConsultationTerms);
            model.addAttribute("regularConsultationTerms", regularConsultationTerms);
            return "consultationsDisplay/professorConsultations";
        }

        Page<Consultation> regularConsultationTerms = this.consultationService
                .consultationTermsByProfessor(professorId, "WEEKLY","UPCOMING","ACTIVE", regularResults, regularPageNum);
        Page<Consultation> irregularConsultationTerms = this.consultationService
                .consultationTermsByProfessor(professorId, "ONE_TIME","UPCOMING", "ACTIVE", irregularResults, irregularPageNum);

        Map<Long, Boolean> consultationAvailability = new HashMap<>();
        Map<Long, ConsultationAttendance> consultationAttendanceMap = new HashMap<>();
        for (Consultation consultation : regularConsultationTerms) {
            consultationAvailability.put(consultation.getId(), isMoreThan2HoursAway(consultation));
            ConsultationAttendance attendance = this.consultationAttendanceService.getAttendanceByConsultationIdAndStudentId(consultation.getId(), authentication.getName());
            if (attendance != null) {
                consultationAttendanceMap.put(consultation.getId(), attendance);
            }
        }
        for (Consultation consultation : irregularConsultationTerms) {
            consultationAvailability.put(consultation.getId(), isMoreThan2HoursAway(consultation));
            ConsultationAttendance attendance = this.consultationAttendanceService.getAttendanceByConsultationIdAndStudentId(consultation.getId(), authentication.getName());
            if (attendance != null) {
                consultationAttendanceMap.put(consultation.getId(), attendance);
            }
        }

        model.addAttribute("isAuthenticated", isAuthenticated);
        model.addAttribute("consultationAttendanceMap", consultationAttendanceMap);
        model.addAttribute("email", studentEmail);
        model.addAttribute("regularPage", regularConsultationTerms);
        model.addAttribute("irregularPage", irregularConsultationTerms);
        model.addAttribute("consultationAvailability", consultationAvailability);

        return "consultationsDisplay/consultations";
    }

    @PostMapping("/schedule")
    public String scheduleConsultation(@RequestParam("consultationTermId") Long consultationId,
                                       @RequestParam("comment") String comment,
                                       @RequestParam("professorId") String professorId,
                                       RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        try {
            consultationAttendanceService.registerStudentForConsultationTerm(consultationId, authentication.getName(), comment);
            redirectAttributes.addFlashAttribute("success", "You have successfully registered for the consultation.");
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("error", "An error occurred while registering for the consultation.");
        }
        return "redirect:/consultations/professor/" + professorId;
    }

    @PostMapping("/cancel")
    public String cancelAttendance(@RequestParam Long attendanceId,
                                   @RequestParam("professorId") String professorId,
                                   RedirectAttributes redirectAttributes) {
        try {
            consultationAttendanceService.deleteConsultationAttendanceById(attendanceId);
            redirectAttributes.addFlashAttribute("successMessage", "Успешно го откажавте присуството.");
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("errorMessage", "Грешка при откажувањето на консултацијата.");
        }

        return "redirect:/consultations/professor/" + professorId;
    }

    @PostMapping("/addComment")
    public String addComment(@RequestParam Long attendanceId,
                                   @RequestParam("professorId") String professorId,
                                   @RequestParam("newComment") String comment,
                                    @RequestParam("consultationId") String consultationId,
                                   RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        boolean isStudent = authentication.getAuthorities().stream().anyMatch(auth -> auth.getAuthority().equals("ROLE_STUDENT"));
        try {
            consultationAttendanceService.addAttendanceComment(attendanceId, authentication.getName(), comment, isStudent);
            redirectAttributes.addFlashAttribute("successMessage", "Успешно додадовте коментар.");
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("errorMessage", "Грешка при додавање на коментар.");
        }
        if(!consultationId.isEmpty()){
            return "redirect:/manage-consultations/"+professorId+"/consultation-details/"+consultationId;
        }
        if(professorId.isEmpty()){
            if(isStudent)
                return "redirect:/studentAttendances";
            return "redirect:/professorAttendances";
        }

        return "redirect:/consultations/professor/" + professorId;
    }


    private Student getStudent() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if (principal instanceof FacultyUserDetails) {
            if (((FacultyUserDetails) principal).getUser().getRole().equals(UserRole.STUDENT)) {
                return ((FacultyUserDetails) principal).getStudent();
            }
        }
        return null;
    }

    private String getStudentEmail() {
        Student student = getStudent();
        return (student != null) ? student.getEmail() : null;
    }

    public boolean isMoreThan2HoursAway(Consultation consultation) {
        LocalDateTime consultationDateTime = LocalDateTime.of(consultation.getOneTimeDate(), consultation.getStartTime());
        LocalDateTime now = LocalDateTime.now();
        return now.isBefore(consultationDateTime.minusHours(2));
    }

}