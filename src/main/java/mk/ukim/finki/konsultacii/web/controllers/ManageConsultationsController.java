package mk.ukim.finki.konsultacii.web.controllers;

import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import mk.ukim.finki.konsultacii.model.Consultation;
import mk.ukim.finki.konsultacii.model.ConsultationAttendance;
import mk.ukim.finki.konsultacii.model.Room;
import mk.ukim.finki.konsultacii.model.dtos.*;
import mk.ukim.finki.konsultacii.repository.ImportRepository;
import mk.ukim.finki.konsultacii.service.*;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import java.io.IOException;
import java.io.OutputStream;
import java.time.DayOfWeek;
import java.util.*;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/manage-consultations")
@AllArgsConstructor
public class ManageConsultationsController {

    private final RoomService roomService;

    private final ConsultationAttendanceService consultationAttendanceService;

    private final RegularConsultationTermService regularConsultationTermService;

    private final IrregularConsultationTermService irregularConsultationTermService;

    private final ConsultationService consultationService;

    private final ImportRepository importRepository;

    @GetMapping(value="/{professorId}", params = {"!time", "!status"})
    public String getConsultationPage(Model model,
                                      @ModelAttribute("errorMessage") String errorMessage,
                                      @ModelAttribute("successMessage") String successMessage,
                                      @RequestParam(required = false) String type,
                                      @RequestParam(defaultValue = "1") Integer pageNum,
                                      @RequestParam(defaultValue = "10") Integer results,
                                      @PathVariable("professorId") String professorId,
                                      RedirectAttributesModelMap redirectModel
    ) {
        redirectModel.put("errorMessage", errorMessage);
        redirectModel.put("successMessage", successMessage);
        redirectModel.put("type", type);
        redirectModel.put("pageNum", pageNum);
        redirectModel.put("results", results);
        redirectModel.put("time", "UPCOMING");
        redirectModel.put("status", "ACTIVE");
        return "redirect:/manage-consultations/" + professorId;
    }

    @GetMapping(value="/{professorId}", params = {"time", "status"})
    public String getConsultationPage(Model model,
                                      @ModelAttribute("errorMessage") String errorMessage,
                                      @ModelAttribute("successMessage") String successMessage,
                                      @RequestParam(required = false) String type,
                                      @RequestParam(required = true) String time,
                                      @RequestParam(required = true) String status,
                                      @RequestParam(defaultValue = "1") Integer pageNum,
                                      @RequestParam(defaultValue = "10") Integer results,
                                      @PathVariable("professorId") String professorId,
                                      RedirectAttributes redirectAttributes
    ) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        List<DayOfWeek> daysOfWeek = Arrays.stream(DayOfWeek.values()).toList();
        List<Room> rooms = roomService.getAllRooms();

        Page<Consultation> consultations = this.consultationService.consultationTermsByProfessor(professorId, type, time, status, results, pageNum);
        Map<Long, Long> attendanceCountMap = consultationService.getAttendanceCountByConsultationId();

        model.addAttribute("page", consultations);
        model.addAttribute("username", username);
        model.addAttribute("professorId", professorId);
        model.addAttribute("daysOfWeek", daysOfWeek);
        model.addAttribute("rooms", rooms);
        model.addAttribute("errorMessage", errorMessage);
        model.addAttribute("successMessage", successMessage);
        model.addAttribute("attendanceCountMap", attendanceCountMap);

        return "manageConsultations/manageConsultations";
    }

    private void setRedirectAttributesErrorMessage(BindingResult result, RedirectAttributes redirectAttributes) {
        redirectAttributes.addAttribute("errorMessage",
                result.getAllErrors().stream()
                        .map(DefaultMessageSourceResolvable::getDefaultMessage)
                        .collect(Collectors.joining("\n")));
    }

    @PostMapping("/{professorId}/create-regular-consultations")
    public String createRegularConsultations(@Validated RegularConsultationFormDto regularConsultationFormDto,
                                             BindingResult result,
                                             RedirectAttributes attributes,
                                             @PathVariable("professorId") String professorId
    ) {
        if (result.hasErrors()) {
            setRedirectAttributesErrorMessage(result, attributes);
            return "redirect:/manage-consultations/"+ professorId;
        }
        attributes.addAttribute("successMessage", "Успешно креирани термини за редовни консултации!");
        regularConsultationTermService.create(regularConsultationFormDto, professorId);
        return "redirect:/manage-consultations/"+ professorId;
    }

    @PostMapping("/{professorId}/create-irregular-consultations")
    public String createIrregularConsultations(@Validated IrregularConsultationsFormDto irregularConsultationsFormDto,
                                               BindingResult result,
                                               RedirectAttributes attributes,
                                               @PathVariable("professorId") String professorId
    ) {
        if (result.hasErrors()) {
            setRedirectAttributesErrorMessage(result, attributes);
            return "redirect:/manage-consultations/"+ professorId;
        }
        irregularConsultationTermService.create(irregularConsultationsFormDto, professorId);
        attributes.addAttribute("successMessage", "Успешно креиран термин за дополнителни консултации!");
        return "redirect:/manage-consultations/"+ professorId;
    }

    @GetMapping("/{professorId}/edit-consultation/{id}")
    public String editConsultation(@PathVariable("id") Long consultationId,
                                   @PathVariable("professorId") String professorId,
                                   @ModelAttribute("errorMessage") String errorMessage,
                                   @ModelAttribute("successMessage") String successMessage,
                                   Model model) {
        Consultation consultation = consultationService.getConsultationDetails(consultationId);
        if (consultation == null) {
            return "error";
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        model.addAttribute("daysOfWeek", DayOfWeek.values());
        model.addAttribute("rooms", roomService.getAllRooms());
        model.addAttribute("username", username);
        model.addAttribute("professorId", professorId);
        model.addAttribute("consultation", consultation);
        model.addAttribute("errorMessage", errorMessage);
        model.addAttribute("successMessage", successMessage);
        return "manageConsultations/editConsultation";
    }

    @PostMapping("/{professorId}/update-consultation/{id}")
    public String updateConsultation(@PathVariable Long id,
                                     @Validated IrregularConsultationsFormDto irregularConsultationsFormDto,
                                     @PathVariable("professorId") String professorId,
                                     BindingResult result,
                                     RedirectAttributes attributes) {
        if (result.hasErrors()) {
            setRedirectAttributesErrorMessage(result, attributes);
            return "redirect:/manage-consultations/"+professorId+"/edit-consultation/"+id;
        }
        irregularConsultationTermService.edit(id, irregularConsultationsFormDto);
        attributes.addAttribute("successMessage", "Успешно го ажуриравте терминот за консултации.");
        return "redirect:/manage-consultations/"+professorId+"/consultation-details/"+id;
    }

    @PostMapping("/{professorId}/updateAllUpcomingWeekly/{id}")
    public String updateWeeklyConsultations(@PathVariable Long id,
                                     @Validated RegularConsultationFormDto regularConsultationsFormDto,
                                     @PathVariable("professorId") String professorId,
                                     BindingResult result,
                                     RedirectAttributes attributes) {
        if (result.hasErrors()) {
            setRedirectAttributesErrorMessage(result, attributes);
            return "redirect:/manage-consultations/"+professorId+"/edit-consultation/"+id;
        }
        consultationService.editAllWeeklyConsultations(id, regularConsultationsFormDto);
        attributes.addAttribute("successMessage", "Успешно ги ажуриравте термините за редовни консултации.");
        return "redirect:/manage-consultations/"+professorId;
    }

    @PostMapping("/{professorId}/delete/{id}")
    public String deleteConsultation(@PathVariable("id") Long consultationId,
                                     @PathVariable("professorId") String professorId,
                                     RedirectAttributes redirectAttributes) {
        try {
            consultationService.deleteConsultation(consultationId);
            redirectAttributes.addFlashAttribute("successMessage", "Consultation deleted successfully");
        } catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
        }

        return "redirect:/manage-consultations/"+professorId;
    }

    @PostMapping("/{professorId}/deleteAllUpcomingWeekly/{id}")
    public String deleteWeeklyConsultations(@PathVariable("id") Long consultationId,
                                            @PathVariable("professorId") String professorId,
                                            RedirectAttributes redirectAttributes) {
        try {
            consultationService.deleteAllUpcomingWeeklyConsultations(consultationId);
            redirectAttributes.addFlashAttribute("successMessage", "Сите наредни редовни консултации од терминот се успешно избришани.");
        } catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
        }

        return "redirect:/manage-consultations/"+professorId;
    }

    @PostMapping("/{professorId}/cancel/{id}")
    public String cancelConsultation(@PathVariable("id") Long consultationId,
                                     @PathVariable("professorId") String professorId,
                                     RedirectAttributes redirectAttributes) {
        try {
            consultationService.cancelConsultation(consultationId);
            redirectAttributes.addFlashAttribute("successMessage", "Успешно го откажавте терминот за консултации.");
        } catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
        }

        return "redirect:/manage-consultations/"+professorId;
    }

    @PostMapping("/{professorId}/toggle-status/{id}")
    public String toggleStatus(@PathVariable("id") Long consultationId,
                               @PathVariable("professorId") String professorId,
                               RedirectAttributes redirectAttributes) {
        try {
            consultationService.toggleStatus(consultationId);
            redirectAttributes.addFlashAttribute("successMessage", "Успешно го променивте статусот.");
        } catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
        }

        return "redirect:/manage-consultations/"+professorId;
    }

    @GetMapping("/{professorId}/consultation-details/{id}")
    public String getConsultationDetails(@PathVariable("id") Long consultationId,
                                         @PathVariable("professorId") String professorId,
                                         Model model,
                                         @ModelAttribute("errorMessage") String errorMessage,
                                         @ModelAttribute("successMessage") String successMessage) {
        Consultation consultation = consultationService.getConsultationDetails(consultationId);
        if (consultation == null) {
            return "error";
        }

        List<ConsultationAttendance> attendees = consultationAttendanceService.getAttendancesByConsultationId(consultationId);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        model.addAttribute("errorMessage", errorMessage);
        model.addAttribute("successMessage", successMessage);
        model.addAttribute("username", username);
        model.addAttribute("consultation", consultation);
        model.addAttribute("attendees", attendees);
        model.addAttribute("attendanceCount", attendees.size());
        return "manageConsultations/consultationDetails";
    }

    @GetMapping("/sample-tsv")
    public void sampleTsv(HttpServletResponse response) {
        List<ConsultationDto> example = List.of(
                new ConsultationDto("dimitar.trajanov", "Б 2.2", "15.11.2024", "", "14:30", "15:30", "false", "test1", null),
                new ConsultationDto("riste.stojanov", "Амф П", "", "MONDAY", "10:00", "11:00", "true", "", null)
        );

        String fileName = "consultations_template.tsv";
        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(ConsultationDto.class, example, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/import")
    public void importConsultations(@RequestParam("file") MultipartFile file,
                                             HttpServletResponse response) {
        List<ConsultationDto> consultations = importRepository.readTypeList(file, ConsultationDto.class);

        List<ConsultationDto> invalidConsultations = consultationService.importConsultations(consultations);

        String fileName = "invalid_consultations.tsv";
        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(ConsultationDto.class, invalidConsultations, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
