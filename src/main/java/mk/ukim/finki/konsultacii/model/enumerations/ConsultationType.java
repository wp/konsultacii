package mk.ukim.finki.konsultacii.model.enumerations;

public enum ConsultationType {
    WEEKLY, ONE_TIME
}
