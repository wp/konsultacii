package mk.ukim.finki.konsultacii.model.enumerations;

public enum ConsultationStatus {
    ACTIVE,
    INACTIVE
}
