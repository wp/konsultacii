package mk.ukim.finki.konsultacii.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import mk.ukim.finki.konsultacii.model.Consultation;

import java.util.List;


@Getter
@AllArgsConstructor
public class IrregularConsultationDto {
    Consultation irregularConsultationTerm;
    Long numberOfAttendants;
    List<UserAttendanceDto> registeredStudents;
}
