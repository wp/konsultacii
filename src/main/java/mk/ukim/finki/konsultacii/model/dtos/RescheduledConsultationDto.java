package mk.ukim.finki.konsultacii.model.dtos;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalTime;


@Getter
@Setter
public class RescheduledConsultationDto extends RegularConsultationFormDto {
    @NotNull(message = "Мора да биде специфицирана регуларна консултација")
    private Long regularConsultationId;

    public RescheduledConsultationDto(
            LocalTime startTime, LocalTime endTime, String roomName, URL link,
            DayOfWeek dayOfWeek, Long regularConsultationId, Boolean isOnline, String studentInstructions) {
        super(startTime, endTime, roomName, isOnline, studentInstructions, link, dayOfWeek);
        this.regularConsultationId = regularConsultationId;
    }
}
