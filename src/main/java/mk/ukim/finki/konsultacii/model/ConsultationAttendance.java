package mk.ukim.finki.konsultacii.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;


@Entity
@Table(name = "consultation_attendance")
@Getter
@Setter
public class ConsultationAttendance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Student attendee;
    @Getter
    @ManyToOne
    private Consultation consultation;

    @Column(length = 1000)
    private String comment;

    private Boolean reportAbsentProfessor;

    private Boolean reportAbsentStudent;

    private String absentProfessorComment;

    private String absentStudentComment;

    public ConsultationAttendance(Student attendee, Consultation consultation, String comment) {
        this.attendee = attendee;
        this.consultation = consultation;
        this.comment = comment;
    }

    public ConsultationAttendance() {

    }


    public boolean isStarted() {
        return this.consultation.isStarted();
    }

    public boolean isActive() {
        return this.consultation.isActive();
    }

    public boolean isEnded() {
        return this.consultation.isEnded();
    }

    public LocalTime getConsultationStartTime(){
        return this.consultation.getStartTime();
    }

    public LocalDate getConsultationOneTimeDate(){
        return this.consultation.getOneTimeDate();
    }
}
