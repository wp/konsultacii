package mk.ukim.finki.konsultacii.model.exceptions;

public class OverlappingConsultationException extends RuntimeException{

    public OverlappingConsultationException(String message){
        super(message);
    }

}
