package mk.ukim.finki.konsultacii.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import mk.ukim.finki.konsultacii.model.Room;
import mk.ukim.finki.konsultacii.model.projections.UserAttendanceProjection;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.List;


@Getter
@AllArgsConstructor
public class RegularConsultationTermInfoDto {
    private DayOfWeek dayOfWeek;
    private LocalTime startTime;
    private LocalTime endTime;
    private Room room;
    private List<UserAttendanceProjection> attendeesCurrentWeek;
}
