package mk.ukim.finki.konsultacii.model.dtos;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class UserAttendanceDto {
    private String email;

    private String firstName;

    private String lastName;

    private String comment;

    public UserAttendanceDto(String email, String firstName, String lastName, String comment) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.comment = comment;
    }
}
