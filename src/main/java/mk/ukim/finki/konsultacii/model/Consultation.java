package mk.ukim.finki.konsultacii.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import mk.ukim.finki.konsultacii.model.enumerations.ConsultationStatus;
import mk.ukim.finki.konsultacii.model.enumerations.ConsultationType;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;


@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
public class Consultation {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Professor professor;

    @ManyToOne
    private Room room;

    @Enumerated(EnumType.STRING)
    private ConsultationType type;

    private LocalDate oneTimeDate;

    @Enumerated(EnumType.STRING)
    private DayOfWeek weeklyDayOfWeek;

    private LocalTime startTime;

    private LocalTime endTime;

    @ElementCollection
    private List<LocalDate> canceledDates;

    @Enumerated(EnumType.STRING)
    private ConsultationStatus status;

    private Boolean online;

    private String studentInstructions;

    public Consultation(Professor professor, Room room, ConsultationType type,
                        LocalDate date, DayOfWeek dayOfWeek, LocalTime timeFrom, LocalTime timeTo,
                        Boolean online, String studentInstructions) {
        this.professor = professor;
        this.room = room;
        this.type = type;
        this.oneTimeDate = date;
        this.weeklyDayOfWeek = dayOfWeek;
        this.startTime = timeFrom;
        this.endTime = timeTo;
        this.status = ConsultationStatus.ACTIVE;
        this.online = online;
        this.studentInstructions = studentInstructions;
    }

    public LocalDateTime getOneTimeStart() {
        return this.oneTimeDate.atTime(this.startTime);
    }


    public boolean isStarted() {
        return LocalDateTime.now().isAfter(this.getOneTimeStart());
    }

    public boolean isActive() {
        return this.status == ConsultationStatus.ACTIVE;
    }

    public boolean isEnded() {
        return LocalDateTime.now().isAfter(this.oneTimeDate.atTime(this.endTime));
    }

    public DayOfWeek getDayOfWeek() {
        return this.weeklyDayOfWeek;
    }

    public LocalTime getTimeFrom() {
        return this.startTime;
    }

    public LocalTime getTimeTo() {
        return this.endTime;
    }
}

