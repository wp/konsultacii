package mk.ukim.finki.konsultacii.model.projections;

public interface UserAttendanceProjection {
    String getEmail();

    String getFirstName();

    String getLastName();

    String getComment();
}
