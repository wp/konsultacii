package mk.ukim.finki.konsultacii.model.enumerations;

public enum RoomType {
    CLASSROOM,
    LAB,
    MEETING_ROOM,
    OFFICE,
    VIRTUAL
}
