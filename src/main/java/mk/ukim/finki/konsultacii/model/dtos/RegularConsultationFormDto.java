package mk.ukim.finki.konsultacii.model.dtos;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalTime;


@Getter
@Setter
public class RegularConsultationFormDto extends ConsultationFormDto {
    @NotNull(message = "Денот во неделата за регуларни консултации мора да биде специфициран")
    DayOfWeek dayOfWeek;

    public RegularConsultationFormDto(
            LocalTime startTime, LocalTime endTime, String roomName, Boolean online,
            String studentInstructions, URL link, DayOfWeek dayOfWeek) {
        super(startTime, endTime, online, studentInstructions, roomName, link);
        this.dayOfWeek = dayOfWeek;
    }
}
