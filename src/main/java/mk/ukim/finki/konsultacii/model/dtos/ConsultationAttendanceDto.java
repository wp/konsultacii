package mk.ukim.finki.konsultacii.model.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;


@Getter
public class ConsultationAttendanceDto {
    private final String attendee;
    private final Long consultationTermId;
    private final String comment;

    @JsonCreator
    public ConsultationAttendanceDto(@JsonProperty("consultationTermId") Long consultationTermId,
                                     @JsonProperty("attendee") String attendee,
                                     @JsonProperty("comment") String comment) {
        this.consultationTermId = consultationTermId;
        this.attendee = attendee;
        this.comment = comment;
    }
}
