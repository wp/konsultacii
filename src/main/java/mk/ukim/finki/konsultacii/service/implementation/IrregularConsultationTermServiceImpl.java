package mk.ukim.finki.konsultacii.service.implementation;

import lombok.AllArgsConstructor;
import mk.ukim.finki.konsultacii.model.Consultation;
import mk.ukim.finki.konsultacii.model.Professor;
import mk.ukim.finki.konsultacii.model.Room;
import mk.ukim.finki.konsultacii.model.dtos.IrregularConsultationsFormDto;
import mk.ukim.finki.konsultacii.model.dtos.MailSendingStatus;
import mk.ukim.finki.konsultacii.model.enumerations.ConsultationType;
import mk.ukim.finki.konsultacii.model.exceptions.IrregularConsultationTermNotFoundException;
import mk.ukim.finki.konsultacii.model.exceptions.ProfessorNotFoundException;
import mk.ukim.finki.konsultacii.repository.ConsultationAttendanceRepository;
import mk.ukim.finki.konsultacii.repository.ConsultationRepository;
import mk.ukim.finki.konsultacii.repository.ProfessorRepository;
import mk.ukim.finki.konsultacii.service.IrregularConsultationTermService;
import mk.ukim.finki.konsultacii.service.RoomService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.CompletableFuture;


@Service
@AllArgsConstructor
public class IrregularConsultationTermServiceImpl implements IrregularConsultationTermService {

    private final ConsultationRepository consultationRepository;
    private final ProfessorRepository professorRepository;
    private final RoomService roomService;
    private final NotificationService notificationService;

    @Override
    public Consultation create(IrregularConsultationsFormDto dto, String professorEmail) {
        Room room = roomService.getByName(dto.getRoomName(), dto.getLink());
        Professor professor = professorRepository.findById(professorEmail).orElseThrow(ProfessorNotFoundException::new);

        return consultationRepository.save(new Consultation(professor, room, ConsultationType.ONE_TIME,
                dto.getDate(), dto.getDate().getDayOfWeek(), dto.getStartTime(), dto.getEndTime(),
                dto.getOnline(), dto.getStudentInstructions()));
    }

    @Override
    public List<CompletableFuture<MailSendingStatus>>  edit(Long id, IrregularConsultationsFormDto dto) {
        Consultation irregularConsultationTerm = consultationRepository
                .findById(id).orElseThrow(() -> new IrregularConsultationTermNotFoundException(id));
        Room room = roomService.getByName(dto.getRoomName(), dto.getLink());

        irregularConsultationTerm.setOneTimeDate(dto.getDate());
        irregularConsultationTerm.setWeeklyDayOfWeek(dto.getDate().getDayOfWeek());
        irregularConsultationTerm.setStartTime(dto.getStartTime());
        irregularConsultationTerm.setEndTime(dto.getEndTime());
        irregularConsultationTerm.setRoom(room);
        irregularConsultationTerm.setOnline(dto.getOnline());
        irregularConsultationTerm.setStudentInstructions(dto.getStudentInstructions());

        Consultation updatedConsultation = consultationRepository.save(irregularConsultationTerm);
        return notificationService.notifyStudentsAboutUpdatedConsultation(updatedConsultation);
    }

}
