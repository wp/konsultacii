package mk.ukim.finki.konsultacii.service;


import mk.ukim.finki.konsultacii.model.User;

public interface UserService {

    User getUserById(String id);
}
