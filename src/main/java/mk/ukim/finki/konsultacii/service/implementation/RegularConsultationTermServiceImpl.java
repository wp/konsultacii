package mk.ukim.finki.konsultacii.service.implementation;

import lombok.AllArgsConstructor;
import mk.ukim.finki.konsultacii.model.Consultation;
import mk.ukim.finki.konsultacii.model.Professor;
import mk.ukim.finki.konsultacii.model.Room;
import mk.ukim.finki.konsultacii.model.constants.ApplicationConstants;
import mk.ukim.finki.konsultacii.model.dtos.*;
import mk.ukim.finki.konsultacii.model.enumerations.ConsultationStatus;
import mk.ukim.finki.konsultacii.model.enumerations.ConsultationType;
import mk.ukim.finki.konsultacii.model.exceptions.*;
import mk.ukim.finki.konsultacii.repository.ConsultationRepository;
import mk.ukim.finki.konsultacii.repository.ProfessorRepository;
import mk.ukim.finki.konsultacii.service.RegularConsultationTermService;
import mk.ukim.finki.konsultacii.service.RoomService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.TextStyle;
import java.util.*;
import java.util.concurrent.CompletableFuture;


@Service
@AllArgsConstructor
public class RegularConsultationTermServiceImpl implements RegularConsultationTermService {
    private final ConsultationRepository consultationRepository;
    private final ProfessorRepository professorRepository;
    private final RoomService roomService;

    private Map<String, LocalDate> calculateStartDayOfSemester() {
        Map<String, LocalDate> daysMap = new HashMap<>();

        if (ApplicationConstants.WINTER_SEMESTER_DATE_RANGE.isDateInSemesterRange(LocalDate.now(), LocalDate.now().getYear())) {
            daysMap.put("startDay", LocalDate.of(
                    LocalDate.now().getYear(),
                    ApplicationConstants.WINTER_SEMESTER_DATE_RANGE.getStartMonth(),
                    ApplicationConstants.WINTER_SEMESTER_DATE_RANGE.getStartDay()
            ));
            daysMap.put("endDay", LocalDate.of(
                    LocalDate.now().getYear() + 1,
                    ApplicationConstants.WINTER_SEMESTER_DATE_RANGE.getEndMonth(),
                    ApplicationConstants.WINTER_SEMESTER_DATE_RANGE.getEndDay()
            ));
        } else {
            daysMap.put("startDay", LocalDate.of(
                    LocalDate.now().getYear(),
                    ApplicationConstants.SUMMER_SEMESTER_DATE_RANGE.getStartMonth(),
                    ApplicationConstants.SUMMER_SEMESTER_DATE_RANGE.getStartDay()
            ));
            daysMap.put("endDay", LocalDate.of(
                    LocalDate.now().getYear(),
                    ApplicationConstants.SUMMER_SEMESTER_DATE_RANGE.getEndMonth(),
                    ApplicationConstants.SUMMER_SEMESTER_DATE_RANGE.getEndDay()
            ));
        }
        return daysMap;
    }

    @Transactional
    @Override
    public List<Consultation> create(RegularConsultationFormDto dto, String professorEmail) {
        List<Consultation> regularConsultationTerms = new ArrayList<>();
        Room room = roomService.getByName(dto.getRoomName(), dto.getLink());
        Professor createdBy = professorRepository.findById(professorEmail).orElseThrow(ProfessorNotFoundException::new);

        LocalDate startDate = calculateStartDayOfSemester().get("startDay");
        LocalDate endDate = calculateStartDayOfSemester().get("endDay");

        startDate = startDate.isAfter(LocalDate.now()) ? startDate : LocalDate.now();

        while (!startDate.isAfter(endDate)) {
            if (startDate.getDayOfWeek().toString().equals(dto.getDayOfWeek().toString())) {
                Consultation consultation = consultationRepository
                        .save(new Consultation(createdBy, room, ConsultationType.WEEKLY, startDate,
                                dto.getDayOfWeek(), dto.getStartTime(), dto.getEndTime(),
                                dto.getOnline(), dto.getStudentInstructions()));
                regularConsultationTerms.add(consultation);
            }
            startDate = startDate.plusDays(1);
        }
        return regularConsultationTerms;
    }

}