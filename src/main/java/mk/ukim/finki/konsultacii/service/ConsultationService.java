package mk.ukim.finki.konsultacii.service;

import mk.ukim.finki.konsultacii.model.Consultation;
import mk.ukim.finki.konsultacii.model.dtos.ConsultationDto;
import mk.ukim.finki.konsultacii.model.dtos.ConsultationResponseDto;
import mk.ukim.finki.konsultacii.model.dtos.MailSendingStatus;
import mk.ukim.finki.konsultacii.model.dtos.RegularConsultationFormDto;
import mk.ukim.finki.konsultacii.model.enumerations.ConsultationType;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public interface ConsultationService {

    void deleteConsultation(Long consultationId);
    List<Consultation> listConsultationTermsByProfessor(String id, String type, String time, String status);
    List<Consultation> getUpcomingConsultationsByProfessor(String id, String type);
    Page<Consultation> consultationTermsByProfessor(String id, String type, String time, String status, int pageSize, int pageNum);
    List<Consultation> listNextWeekConsultationsByProfessor(String id, ConsultationType type);
    Map<Long, Long> getAttendanceCountByConsultationId();
    Consultation getConsultationDetails(Long consultationId);
    List<ConsultationDto> importConsultations(List<ConsultationDto> consultations);
    void deleteAllUpcomingWeeklyConsultations(Long consultationId);
    List<ConsultationResponseDto> getConsultationsByProfessor(String professorId);
    void cancelConsultation(Long consultationId);
    List<CompletableFuture<MailSendingStatus>> toggleStatus(Long consultationId);
    void editAllWeeklyConsultations(Long consultationId, RegularConsultationFormDto dto);
}
