package mk.ukim.finki.konsultacii.service.implementation;

import lombok.AllArgsConstructor;
import mk.ukim.finki.konsultacii.model.Consultation;
import mk.ukim.finki.konsultacii.model.Student;
import mk.ukim.finki.konsultacii.model.dtos.MailSendingStatus;
import mk.ukim.finki.konsultacii.model.projections.UserAttendanceProjection;
import mk.ukim.finki.konsultacii.repository.ConsultationAttendanceRepository;
import mk.ukim.finki.konsultacii.service.EmailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;


@Service
public class NotificationService {

    private final EmailService emailService;
    private final ConsultationAttendanceRepository consultationRepository;
    private final String baseUrl;

    public NotificationService(EmailService emailService, ConsultationAttendanceRepository consultationRepository,
                               @Value("${app.base-url}") String baseUrl){
        this.emailService = emailService;
        this.baseUrl = baseUrl;
        this.consultationRepository = consultationRepository;
    }

    public List<CompletableFuture<MailSendingStatus>>  notifyStudentsAboutUpdatedConsultation(Consultation consultation) {
        return notifyStudents(consultation, "Известување за промена на консултации",
                "updated-consultation-template");
    }

    public List<CompletableFuture<MailSendingStatus>>  notifyStudentsAboutCanceledConsultation(Consultation consultation) {
        return notifyStudents(consultation, "Известување за откажани консултации",
                "canceled-consultation-template");
    }

    public List<CompletableFuture<MailSendingStatus>>  notifyProfessorAboutStudentAttendance(Consultation consultation, Student student,
                                                                         String professorEmail, String comment) {
        Map<String, Object> model = createModel(consultation);
        model.put("studentName", student.getName()+" "+student.getLastName());
        model.put("index", student.getIndex());
        model.put("comment", comment);
        String subject = "Студент се пријави за консултации";
        String template = "student-attendance-consultation-template";
        String detailsLink = String.format("%s/manage-consultations/%s/consultation-details/%s", baseUrl,
                consultation.getProfessor().getId(), consultation.getId());
        model.put("detailsLink", detailsLink);

        List<CompletableFuture<MailSendingStatus>> mailStatuses = new ArrayList<>();

        mailStatuses.add(emailService.sendMail(new String[]{professorEmail}, subject, template, null, model, null));

        return mailStatuses;
    }

    private List<CompletableFuture<MailSendingStatus>>  notifyStudents(Consultation consultation, String subject, String template) {
        List<UserAttendanceProjection> attendees =
                consultationRepository.findAttendeesForConsultation(consultation.getId());

        Map<String, Object> model = createModel(consultation);
        List<CompletableFuture<MailSendingStatus>>  mailStatuses = new ArrayList<>();
        for (UserAttendanceProjection attendee : attendees) {
            mailStatuses.add(
                    emailService.sendMail(new String[]{attendee.getEmail()}, subject,
                            template, null, model, null)
            );
        }
        return mailStatuses;
    }

    public List<CompletableFuture<MailSendingStatus>>  notifyProfessorCommentAdded(Consultation consultation, String professorName,
                                            String studentEmail, String newComment){
        Map<String, Object> model = createModel(consultation);
        model.put("newComment", newComment);
        model.put("name", professorName);
        List<CompletableFuture<MailSendingStatus>>  mailStatuses = new ArrayList<>();
        mailStatuses.add(emailService.sendMail(new String[]{studentEmail},
                "Професорот додаде нов коментар - Консултации",
                "comment-added-template",
                null, model, null));
        return mailStatuses;
    }

    public List<CompletableFuture<MailSendingStatus>>   notifyStudentCommentAdded(Consultation consultation, String studentName,
                                          String professorEmail, String newComment) {
        Map<String, Object> model = createModel(consultation);
        model.put("newComment", newComment);
        model.put("name", studentName);
        List<CompletableFuture<MailSendingStatus>>  mailStatuses = new ArrayList<>();
        mailStatuses.add(emailService.sendMail(new String[]{professorEmail},
                "Студентот додаде нов коментар - Консултации",
                "comment-added-template",
                null, model, null));
        return mailStatuses;
    }


    private Map<String, Object> createModel(Consultation consultation) {
        Map<String, Object> model = new HashMap<>();
        model.put("professor", consultation.getProfessor().getName());
        model.put("date", consultation.getOneTimeDate());
        model.put("startTime", consultation.getStartTime());
        model.put("endTime", consultation.getEndTime());
        model.put("room", consultation.getRoom().getName());
        model.put("instructions", consultation.getStudentInstructions());
        return model;
    }
}
