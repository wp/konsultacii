package mk.ukim.finki.konsultacii.service.implementation;

import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import mk.ukim.finki.konsultacii.model.Consultation;
import mk.ukim.finki.konsultacii.model.Professor;
import mk.ukim.finki.konsultacii.model.Room;
import mk.ukim.finki.konsultacii.model.constants.ApplicationConstants;
import mk.ukim.finki.konsultacii.model.custom.DayMapper;
import mk.ukim.finki.konsultacii.model.dtos.ConsultationDto;
import mk.ukim.finki.konsultacii.model.dtos.ConsultationResponseDto;
import mk.ukim.finki.konsultacii.model.dtos.MailSendingStatus;
import mk.ukim.finki.konsultacii.model.dtos.RegularConsultationFormDto;
import mk.ukim.finki.konsultacii.model.enumerations.ConsultationStatus;
import mk.ukim.finki.konsultacii.model.enumerations.ConsultationType;
import mk.ukim.finki.konsultacii.model.exceptions.ConsultationTermNotFoundException;
import mk.ukim.finki.konsultacii.model.exceptions.IrregularConsultationTermNotFoundException;
import mk.ukim.finki.konsultacii.model.exceptions.RoomNotFoundException;
import mk.ukim.finki.konsultacii.repository.ConsultationAttendanceRepository;
import mk.ukim.finki.konsultacii.repository.ConsultationRepository;
import mk.ukim.finki.konsultacii.repository.ProfessorRepository;
import mk.ukim.finki.konsultacii.repository.RoomRepository;
import mk.ukim.finki.konsultacii.service.ConsultationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static mk.ukim.finki.konsultacii.specifications.FieldFilterSpecification.*;

@Service
@AllArgsConstructor
public class ConsultationServiceImpl implements ConsultationService {
    private final ConsultationRepository consultationRepository;
    private final ConsultationAttendanceRepository consultationAttendanceRepository;
    private final ProfessorRepository professorRepository;
    private final RoomRepository roomRepository;
    private final NotificationService notificationService;

    @Override
    public void deleteConsultation(Long consultationId) {
        if (consultationRepository.existsById(consultationId)) {
            consultationRepository.deleteById(consultationId);
        } else {
            throw new IllegalArgumentException("Consultation not found");
        }
    }

    @Override
    public void deleteAllUpcomingWeeklyConsultations(Long consultationId) {
        Consultation startConsultation = consultationRepository.findById(consultationId)
                .orElseThrow(() -> new ConsultationTermNotFoundException(consultationId));

        List<Consultation> matchingConsultations = consultationRepository.findAllByProfessor_IdAndTypeAndWeeklyDayOfWeekAndStartTimeAndEndTimeAndOneTimeDateAfter(
                startConsultation.getProfessor().getId(),
                ConsultationType.WEEKLY,
                startConsultation.getWeeklyDayOfWeek(),
                startConsultation.getStartTime(),
                startConsultation.getEndTime(),
                startConsultation.getOneTimeDate().minusDays(1)
        );

        for (Consultation consultation : matchingConsultations) {
            long attendanceCount = consultationAttendanceRepository.attendancesCountByConsultationId(consultation.getId());
            if (attendanceCount > 0) {
                String consultationDate = consultation.getOneTimeDate().toString();
                throw new IllegalArgumentException("Не може да биде избришан терминот на " + consultationDate + " бидејќи има пријавени студенти.");
            } else {
                consultationRepository.delete(consultation);
            }
        }
    }

    @Override
    public List<ConsultationResponseDto> getConsultationsByProfessor(String professorId) {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        LocalDate today = LocalDate.now();
        LocalDate nextWeek = today.plusDays(6);
        LocalDate nextTwoWeeks = today.plusWeeks(2);

        return consultationRepository.findNextWeekActiveConsultations(professorId, today, nextWeek, nextTwoWeeks)
                .stream()
                .map(consultation -> {
                    var startDateTime = consultation.getOneTimeDate().atTime(consultation.getStartTime());
                    var endDateTime = consultation.getOneTimeDate().atTime(consultation.getEndTime());

                    return new ConsultationResponseDto(
                            //title
                            consultation.getProfessor().getName() + "-" + consultation.getRoom().getName(),
                            // start
                            "/Date(" + startDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli() + ")/",
                            // end
                            "/Date(" + endDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli() + ")/",
                            // numDeactivated
                            consultation.getCanceledDates().size(),
                            // id
                            consultation.getId(),
                            // termType
                            consultation.getType().ordinal()+1,
                            // day
                            DayMapper.getMacedonianDay(consultation.getDayOfWeek()),
                            // dayNumber
                            consultation.getDayOfWeek().getValue(),
                            // date
                            "/Date(" + consultation.getOneTimeDate().atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli() + ")/",
                            // timeFrom
                            consultation.getStartTime().toString(),
                            // timeTo
                            consultation.getEndTime().toString(),
                            // classroom
                            consultation.getRoom().getName(),
                            // deactivated
                            consultation.getCanceledDates().stream()
                                    .map(date -> "/Date(" + date.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli() + ")/")
                                    .collect(Collectors.toList()),
                            // deactivatedStr
                            consultation.getCanceledDates().stream()
                                    .map(date -> date.format(dateFormatter))
                                    .collect(Collectors.toList()),
                            // checkedIn
                            0,
                            // courseID
                            null,
                            // dateFormatted
                            consultation.getOneTimeDate().format(dateFormatter)
                    );
                })
                .collect(Collectors.toList());
    }

    @Override
    public void cancelConsultation(Long consultationId) {
        Optional<Consultation> consultationOptional = consultationRepository.findById(consultationId);
        if (consultationOptional.isEmpty()) {
            throw new EntityNotFoundException("Consultation with ID " + consultationId + " not found.");
        }
        Consultation consultation = consultationOptional.get();
        consultation.setStatus(ConsultationStatus.INACTIVE);
        consultationRepository.save(consultation);
    }

    @Override
    public List<CompletableFuture<MailSendingStatus>> toggleStatus(Long consultationId) {
        Optional<Consultation> consultationOptional = consultationRepository.findById(consultationId);
        if (consultationOptional.isEmpty()) {
            throw new EntityNotFoundException("Consultation with ID " + consultationId + " not found.");
        }
        Consultation consultation = consultationOptional.get();
        if (consultation.getStatus() == ConsultationStatus.ACTIVE) {
            consultation.setStatus(ConsultationStatus.INACTIVE);
            consultationRepository.save(consultation);
            return notificationService.notifyStudentsAboutCanceledConsultation(consultation);
        } else {
            consultation.setStatus(ConsultationStatus.ACTIVE);
            consultationRepository.save(consultation);
            return Collections.emptyList();
        }
    }

    @Override
    public void editAllWeeklyConsultations(Long consultationId, RegularConsultationFormDto dto) {
        Consultation startConsultation = consultationRepository
                .findById(consultationId).orElseThrow(() -> new IrregularConsultationTermNotFoundException(consultationId));
        Room room = roomRepository
                .findByName(dto.getRoomName()).orElseThrow(() -> new RoomNotFoundException(dto.getRoomName()));

        List<Consultation> matchingConsultations = consultationRepository.findAllByProfessor_IdAndTypeAndWeeklyDayOfWeekAndStartTimeAndEndTimeAndOneTimeDateAfter(
                startConsultation.getProfessor().getId(),
                ConsultationType.WEEKLY,
                startConsultation.getWeeklyDayOfWeek(),
                startConsultation.getStartTime(),
                startConsultation.getEndTime(),
                startConsultation.getOneTimeDate().minusDays(1)
        );

        DayOfWeek targetDayOfWeek = dto.getDayOfWeek();

        List<Consultation> updatedConsultations = new ArrayList<>();
        for (Consultation consultation : matchingConsultations) {
            LocalDate currentDate = consultation.getOneTimeDate();
            LocalDate newOneTimeDate = calculateNextDateForDayOfWeek(currentDate, targetDayOfWeek);

            consultation.setOneTimeDate(newOneTimeDate);
            consultation.setWeeklyDayOfWeek(targetDayOfWeek);
            consultation.setStartTime(dto.getStartTime());
            consultation.setEndTime(dto.getEndTime());
            consultation.setRoom(room);
            consultation.setOnline(dto.getOnline());
            consultation.setStudentInstructions(dto.getStudentInstructions());
            updatedConsultations.add(consultation);
        }
        consultationRepository.saveAll(updatedConsultations);
    }

    @Override
    public List<Consultation> listConsultationTermsByProfessor(String id, String type, String time, String status) {
        Specification<Consultation> spec = Specification
                .where(filterEquals(Consultation.class, "professor.id", id));

        if (type != null && !type.isEmpty()) {
            spec = spec.and(filterEquals(Consultation.class, "type", ConsultationType.valueOf(type)));
        }

        if (status != null && !status.isEmpty()) {
            spec = spec.and(filterEquals(Consultation.class, "status", ConsultationStatus.valueOf(status)));
        }

        List<Consultation> consultations = this.consultationRepository.findAll(spec);

        ZonedDateTime now = ZonedDateTime.now(TimeZone.getDefault().toZoneId());

        List<Consultation> pastConsultations = consultations.stream()
                .filter(c -> toZonedDateTime(c.getOneTimeDate(), c.getStartTime()).isBefore(now))
                .sorted(Comparator.comparing((Consultation c) -> toZonedDateTime(c.getOneTimeDate(), c.getStartTime()))
                        .reversed())
                .toList();

        List<Consultation> upcomingConsultations = consultations.stream()
                .filter(c -> toZonedDateTime(c.getOneTimeDate(), c.getStartTime()).isAfter(now))
                .sorted(Comparator.comparing((Consultation c) -> toZonedDateTime(c.getOneTimeDate(), c.getStartTime())))
                .toList();

        List<Consultation> sortedConsultations = new ArrayList<>();
        if (time == null || time.isEmpty()) {
            sortedConsultations.addAll(upcomingConsultations);
            sortedConsultations.addAll(pastConsultations);
        } else if (time.equals("UPCOMING")) {
            sortedConsultations = upcomingConsultations;
        } else if (time.equals("PAST")) {
            sortedConsultations = pastConsultations;
        }

        return sortedConsultations;
    }

    @Override
    public List<Consultation> getUpcomingConsultationsByProfessor(String id, String type) {
        LocalDate today = LocalDate.now();
        LocalDate nextThreeWeeks = today.plusWeeks(3);

        return consultationRepository.findConsultationsForNextThreeWeeks(id, ConsultationType.valueOf(type), today, nextThreeWeeks);
    }

    @Override
    public Page<Consultation> consultationTermsByProfessor(String id, String type, String time, String status,
                                                           int pageSize, int pageNum) {
        Specification<Consultation> spec = Specification
                .where(filterEquals(Consultation.class, "professor.id", id));

        if (type != null && !type.isEmpty()) {
            spec = spec.and(filterEquals(Consultation.class, "type", ConsultationType.valueOf(type)));
        }

        if (status != null && !status.isEmpty()) {
            spec = spec.and(filterEquals(Consultation.class, "status", ConsultationStatus.valueOf(status)));
        }

        LocalDateTime now = LocalDateTime.now();

        if ("UPCOMING".equalsIgnoreCase(time)) {
            spec = spec.and(isUpcoming(Consultation.class, "oneTimeDate", "startTime", now));
        } else if ("PAST".equalsIgnoreCase(time)) {
            spec = spec.and(isPast(Consultation.class, "oneTimeDate", "startTime", now));
        }

        Sort sort;
        if ("PAST".equalsIgnoreCase(time)) {
            sort = Sort.by(Sort.Order.desc("oneTimeDate"), Sort.Order.desc("startTime"));
        } else if ("UPCOMING".equalsIgnoreCase(time)) {
            sort = Sort.by(Sort.Order.asc("oneTimeDate"), Sort.Order.asc("startTime"));
        } else {
            sort = Sort.by(Sort.Order.asc("oneTimeDate"), Sort.Order.asc("startTime"));
        }

        Pageable pageable = PageRequest.of(pageNum - 1, pageSize, sort);

        return consultationRepository.findAll(spec, pageable);
    }

    @Override
    public List<Consultation> listNextWeekConsultationsByProfessor(String id, ConsultationType type) {
        LocalDate today = LocalDate.now();

        LocalDate nextWeekEnd = today.plusDays(6);

        return consultationRepository.findConsultationsForNextWeek(
                id,
                type,
                today,
                nextWeekEnd
        );
    }

    @Override
    public Map<Long, Long> getAttendanceCountByConsultationId() {
        List<Object[]> results = consultationAttendanceRepository.countAttendancesByConsultationId();
        return results.stream().collect(Collectors.toMap(
                result -> (Long) result[0],
                result -> (Long) result[1]
        ));
    }

    @Override
    public Consultation getConsultationDetails(Long consultationId) {
        return consultationRepository.findById(consultationId).orElseThrow(()-> new ConsultationTermNotFoundException(consultationId));
    }

    @Override
    public List<ConsultationDto> importConsultations(List<ConsultationDto> consultations) {
        return consultations.stream()
                .map(dto -> saveConsultation(dto))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    private Optional<ConsultationDto> saveConsultation(ConsultationDto dto) {
        try {
            Optional<Professor> professorOpt = professorRepository.findById(dto.getProfessorId());
            Optional<Room> roomOpt = roomRepository.findById(dto.getRoomName());

            if (professorOpt.isEmpty()) {
                dto.setMessage("Invalid professor username.");
                return Optional.of(dto);
            }
            if (roomOpt.isEmpty()){
                dto.setMessage("Invalid room name.");
                return Optional.of(dto);
            }

            if (!dto.getOneTimeDate().isEmpty()) {
                try {
                    String[] consultationDate = dto.getOneTimeDate().split("\\.");
                    LocalDate date = LocalDate.of(
                            Integer.parseInt(consultationDate[2]),
                            Integer.parseInt(consultationDate[1]),
                            Integer.parseInt(consultationDate[0])
                    );
                    consultationRepository.save(
                            new Consultation(
                                    professorOpt.get(),
                                    roomOpt.get(),
                                    ConsultationType.ONE_TIME,
                                    date,
                                    date.getDayOfWeek(),
                                    LocalTime.parse(dto.getStartTime()),
                                    LocalTime.parse(dto.getEndTime()),
                                    Boolean.parseBoolean(dto.getOnline()),
                                    dto.getStudentInstructions()
                            )
                    );
                } catch (Exception e) {
                    dto.setMessage("Invalid date format.");
                    return Optional.of(dto);
                }
            } else {
                LocalDate startDate = calculateStartDayOfSemester().get("startDay");
                LocalDate endDate = calculateStartDayOfSemester().get("endDay");
                startDate = startDate.isAfter(LocalDate.now()) ? startDate : LocalDate.now();
                while (!startDate.isAfter(endDate)) {
                    if (startDate.getDayOfWeek().toString().equals(dto.getWeeklyDayOfWeek())) {
                        consultationRepository.save(
                                new Consultation(
                                        professorOpt.get(),
                                        roomOpt.get(),
                                        ConsultationType.WEEKLY,
                                        startDate,
                                        DayOfWeek.valueOf(dto.getWeeklyDayOfWeek()),
                                        LocalTime.parse(dto.getStartTime()),
                                        LocalTime.parse(dto.getEndTime()),
                                        Boolean.parseBoolean(dto.getOnline()),
                                        dto.getStudentInstructions()));
                    }
                    startDate = startDate.plusDays(1);
                }
            }
            return Optional.empty();

        } catch (Exception e) {
            dto.setMessage("Error saving consultation: " + e.getMessage());
            return Optional.of(dto);
        }
    }

    private Map<String, LocalDate> calculateStartDayOfSemester() {
        Map<String, LocalDate> daysMap = new HashMap<>();

        if (ApplicationConstants.WINTER_SEMESTER_DATE_RANGE.isDateInSemesterRange(LocalDate.now(), LocalDate.now().getYear())) {
            daysMap.put("startDay", LocalDate.of(
                    LocalDate.now().getYear(),
                    ApplicationConstants.WINTER_SEMESTER_DATE_RANGE.getStartMonth(),
                    ApplicationConstants.WINTER_SEMESTER_DATE_RANGE.getStartDay()
            ));
            daysMap.put("endDay", LocalDate.of(
                    LocalDate.now().getYear() + 1,
                    ApplicationConstants.WINTER_SEMESTER_DATE_RANGE.getEndMonth(),
                    ApplicationConstants.WINTER_SEMESTER_DATE_RANGE.getEndDay()
            ));
        } else {
            daysMap.put("startDay", LocalDate.of(
                    LocalDate.now().getYear(),
                    ApplicationConstants.SUMMER_SEMESTER_DATE_RANGE.getStartMonth(),
                    ApplicationConstants.SUMMER_SEMESTER_DATE_RANGE.getStartDay()
            ));
            daysMap.put("endDay", LocalDate.of(
                    LocalDate.now().getYear(),
                    ApplicationConstants.SUMMER_SEMESTER_DATE_RANGE.getEndMonth(),
                    ApplicationConstants.SUMMER_SEMESTER_DATE_RANGE.getEndDay()
            ));
        }
        return daysMap;
    }

    private LocalDate calculateNextDateForDayOfWeek(LocalDate startDate, DayOfWeek targetDayOfWeek) {
        int startDayValue = startDate.getDayOfWeek().getValue();
        int targetDayValue = targetDayOfWeek.getValue();
        int daysToAdd = targetDayValue - startDayValue;

        if (daysToAdd != 0) {
            return startDate.plusDays(daysToAdd);
        } else {
            return startDate;
        }
    }

    private ZonedDateTime toZonedDateTime(LocalDate date, LocalTime time) {
        return ZonedDateTime.of(date, time, TimeZone.getDefault().toZoneId());
    }
}
