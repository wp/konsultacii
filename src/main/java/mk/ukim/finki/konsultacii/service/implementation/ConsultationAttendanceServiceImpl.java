package mk.ukim.finki.konsultacii.service.implementation;

import jakarta.persistence.criteria.Expression;
import lombok.AllArgsConstructor;
import mk.ukim.finki.konsultacii.model.Consultation;
import mk.ukim.finki.konsultacii.model.ConsultationAttendance;
import mk.ukim.finki.konsultacii.model.Student;
import mk.ukim.finki.konsultacii.model.User;
import mk.ukim.finki.konsultacii.model.dtos.MailSendingStatus;
import mk.ukim.finki.konsultacii.model.dtos.StudentScheduledConsultationDto;
import mk.ukim.finki.konsultacii.model.enumerations.ConsultationType;
import mk.ukim.finki.konsultacii.model.exceptions.ConsultationTermNotFoundException;
import mk.ukim.finki.konsultacii.model.exceptions.UserNotFoundException;
import mk.ukim.finki.konsultacii.repository.ConsultationAttendanceRepository;
import mk.ukim.finki.konsultacii.repository.ConsultationRepository;
import mk.ukim.finki.konsultacii.repository.StudentRepository;
import mk.ukim.finki.konsultacii.repository.UserRepository;
import mk.ukim.finki.konsultacii.service.ConsultationAttendanceService;
import mk.ukim.finki.konsultacii.service.ConsultationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static mk.ukim.finki.konsultacii.specifications.FieldFilterSpecification.*;

@Service
@AllArgsConstructor
public class ConsultationAttendanceServiceImpl implements ConsultationAttendanceService {

    private final StudentRepository studentRepository;
    private final ConsultationAttendanceRepository attendsRepository;
    private final ConsultationRepository consultationRepository;
    private final UserRepository userRepository;
    private final NotificationService notificationService;
    private final ConsultationService consultationService;

    @Override
    public List<StudentScheduledConsultationDto> findAllFutureActiveConsultationTermsForStudent(String email) {
        Optional<Student> student = this.studentRepository.findByEmail(email);
        if (student.isEmpty()) {
            return List.of();
        }

        return this.attendsRepository.findAllFutureActiveConsultationsForStudent(student.get().getIndex())
                .stream().map(ct -> {
                    return new StudentScheduledConsultationDto(ct.getId(), ct.getProfessorName(), ct.getOneTimeDate(),
                            ct.getTimeFrom(), ct.getTimeTo(), ct.getRoom(), ct.getComment());
                }).collect(Collectors.toList());
    }

    @Override
    public List<ConsultationAttendance> getAttendancesByConsultationId(Long consultationId) {
        return attendsRepository.findByConsultationId(consultationId);
    }

    @Override
    public ConsultationAttendance getAttendanceByConsultationIdAndStudentId(Long consultationId, String studentId) {
        return attendsRepository.findByConsultation_IdAndAttendee_Index(consultationId, studentId);
    }

    @Override
    public void registerStudentForConsultationTerm(Long consultationId, String index, String comment) {
        Student student = studentRepository.findById(index).orElseThrow(() -> new UserNotFoundException(index));
        Consultation consultation = consultationRepository.findById(consultationId).orElseThrow(() -> new ConsultationTermNotFoundException(consultationId));
        if(Objects.equals(comment, "")){
            attendsRepository.save(new ConsultationAttendance(student,consultation,comment));
        }
        else {
            String initials = student.getName().charAt(0) + student.getLastName().substring(0, 1);
            String dateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
            String fullComment = "--- <br>" + initials + " [" + dateTime + "] " + comment;
            attendsRepository.save(new ConsultationAttendance(student,consultation,fullComment));
        }

        notificationService.notifyProfessorAboutStudentAttendance(consultation,
                student,
                consultation.getProfessor().getEmail(),
                comment);
    }

    @Override
    public List<ConsultationAttendance> listAttendances(String time, String type, String professorId, String studentIndex) {

        Specification<ConsultationAttendance> spec = Specification.where(null);

        if (professorId != null && !professorId.isEmpty()) {
            spec = spec.and(filterEqualsV(ConsultationAttendance.class, "consultation.professor.id", professorId));
        }

        if (studentIndex != null && !studentIndex.isEmpty()) {
            spec = spec.and(filterContainsText(ConsultationAttendance.class, "attendee.index", studentIndex));
        }

        if (type != null && !type.isEmpty()) {
            spec = spec.and(filterEqualsV(ConsultationAttendance.class, "consultation.type", ConsultationType.valueOf(type)));
        }

        List<ConsultationAttendance> attendances = attendsRepository.findAll(spec);

        LocalDateTime now = LocalDateTime.now();

        List<ConsultationAttendance> pastAttendances = attendances.stream()
                .filter(att -> att.getConsultation().getOneTimeDate().atTime(att.getConsultation().getStartTime()).isBefore(now))
                .sorted(Comparator.comparing(ConsultationAttendance::getConsultationOneTimeDate)
                        .thenComparing(ConsultationAttendance::getConsultationStartTime).reversed())
                .toList();

        List<ConsultationAttendance> upcomingAttendances = attendances.stream()
                .filter(att -> att.getConsultation().getOneTimeDate().atTime(att.getConsultation().getStartTime()).isAfter(now))
                .sorted(Comparator.comparing(ConsultationAttendance::getConsultationOneTimeDate)
                        .thenComparing(ConsultationAttendance::getConsultationStartTime))
                .toList();

        List<ConsultationAttendance> sortedAttendances = new ArrayList<>();
        if (time == null || time.isEmpty()) {
            sortedAttendances.addAll(upcomingAttendances);
            sortedAttendances.addAll(pastAttendances);
        } else if ("UPCOMING".equalsIgnoreCase(time)) {
            sortedAttendances = upcomingAttendances;
        } else if ("PAST".equalsIgnoreCase(time)) {
            sortedAttendances = pastAttendances;
        }

        return sortedAttendances;
    }

    @Override
    public Page<ConsultationAttendance> listAttendancesPage(String time, String type, String professorId, String studentIndex,
                                                            int pageSize, int pageNum) {
        Specification<ConsultationAttendance> spec = Specification.where(null);

        if (professorId != null && !professorId.isEmpty()) {
            spec = spec.and(filterEqualsV(ConsultationAttendance.class, "consultation.professor.id", professorId));
        }

        if (studentIndex != null && !studentIndex.isEmpty()) {
            spec = spec.and(filterContainsText(ConsultationAttendance.class, "attendee.index", studentIndex));
        }

        if (type != null && !type.isEmpty()) {
            spec = spec.and(filterEqualsV(ConsultationAttendance.class, "consultation.type", ConsultationType.valueOf(type)));
        }

        LocalDateTime now = LocalDateTime.now();

        if ("UPCOMING".equalsIgnoreCase(time)) {
            spec = spec.and(isUpcoming(ConsultationAttendance.class, "consultation.oneTimeDate", "consultation.startTime", now));
        } else if ("PAST".equalsIgnoreCase(time)) {
            spec = spec.and(isPast(ConsultationAttendance.class, "consultation.oneTimeDate", "consultation.startTime", now));
        }

        Sort sort;
        if ("PAST".equalsIgnoreCase(time)) {
            sort = Sort.by(Sort.Order.desc("consultation.oneTimeDate"), Sort.Order.desc("consultation.startTime"));
        } else if ("UPCOMING".equalsIgnoreCase(time)) {
            sort = Sort.by(Sort.Order.asc("consultation.oneTimeDate"), Sort.Order.asc("consultation.startTime"));
        } else {
            sort = Sort.by(Sort.Order.asc("consultation.oneTimeDate"), Sort.Order.asc("consultation.startTime"));
        }

        Pageable pageable = PageRequest.of(pageNum - 1, pageSize, sort);

        return attendsRepository.findAll(spec, pageable);

    }

    @Override
    public void reportAbsentProfessor(Long attendanceId, Boolean reportAbsentProfessor, String absentProfessorComment) {
        ConsultationAttendance attendance = attendsRepository.findById(attendanceId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid attendance ID"));

        if (reportAbsentProfessor != null && reportAbsentProfessor) {
            attendance.setReportAbsentProfessor(true);
            attendance.setAbsentProfessorComment(absentProfessorComment);
        }

        attendsRepository.save(attendance);
    }

    @Override
    public void reportAbsentStudent(Long attendanceId, Boolean reportAbsentStudent, String absentStudentComment) {
        ConsultationAttendance attendance = attendsRepository.findById(attendanceId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid attendance ID"));

        if (reportAbsentStudent != null && reportAbsentStudent) {
            attendance.setReportAbsentStudent(true);
            attendance.setAbsentStudentComment(absentStudentComment);
        }

        attendsRepository.save(attendance);
    }

    @Override
    public void deleteConsultationAttendanceById(Long attendanceId) {
        attendsRepository.deleteById(attendanceId);
    }

    @Override
    public List<CompletableFuture<MailSendingStatus>>  addAttendanceComment(Long attendanceId, String username, String comment, boolean isStudent) {
        ConsultationAttendance attendance = attendsRepository.findById(attendanceId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid attendance ID"));
        String existingComment = attendance.getComment();
        String dateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        String fullName;
        Student student;
        User user;
        if(isStudent){
            student = studentRepository.findById(username).orElseThrow(() -> new UserNotFoundException(username));
            fullName = student.getName()+" "+student.getLastName();
        }
        else{
            user = userRepository.findById(username).orElseThrow(() -> new UserNotFoundException(username));
            fullName = user.getName();
        }
        String[] nameParts = fullName.split(" ");

        StringBuilder initialsBuilder = new StringBuilder();
        for (String part : nameParts) {
            if (!part.isEmpty()) {
                initialsBuilder.append(part.charAt(0));
            }
        }
        String initials = initialsBuilder.toString().toUpperCase();
        String fullComment = "--- <br>" + initials + " [" + dateTime + "] " + comment;

        String updatedComment = existingComment + " <br> " + fullComment;

        attendance.setComment(updatedComment);
        attendsRepository.save(attendance);
        Consultation consultation = consultationService.getConsultationDetails(attendance.getConsultation().getId());

        if(isStudent){
            return notificationService.notifyStudentCommentAdded(
                    consultation,
                    fullName,
                    consultation.getProfessor().getEmail(),
                    comment
            );
        }
        else{
            return notificationService.notifyProfessorCommentAdded(
                    consultation,
                    fullName,
                    attendance.getAttendee().getEmail(),
                    comment
            );
        }
    }
}