package mk.ukim.finki.konsultacii.service;

import mk.ukim.finki.konsultacii.model.ConsultationAttendance;
import mk.ukim.finki.konsultacii.model.Student;
import mk.ukim.finki.konsultacii.model.dtos.MailSendingStatus;
import mk.ukim.finki.konsultacii.model.dtos.StudentScheduledConsultationDto;
import mk.ukim.finki.konsultacii.model.dtos.UserAttendanceDto;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.concurrent.CompletableFuture;


public interface ConsultationAttendanceService {

    List<StudentScheduledConsultationDto> findAllFutureActiveConsultationTermsForStudent(String email);

    List<ConsultationAttendance> getAttendancesByConsultationId(Long consultationId);

    ConsultationAttendance getAttendanceByConsultationIdAndStudentId(Long consultationId, String studentId);

    void registerStudentForConsultationTerm(Long consultationId, String index, String comment);

    List<ConsultationAttendance> listAttendances(String time, String type, String professorId, String studentIndex);

    Page<ConsultationAttendance> listAttendancesPage(String time, String type, String professorId, String studentIndex,
                                                        int pageSize, int pageNum);

    void reportAbsentProfessor(Long attendanceId, Boolean reportAbsentProfessor, String absentProfessorComment);

    void reportAbsentStudent(Long attendanceId, Boolean reportAbsentStudent, String absentStudentComment);

    void deleteConsultationAttendanceById(Long attendanceId);

    List<CompletableFuture<MailSendingStatus>> addAttendanceComment(Long attendanceId, String username, String comment, boolean isStudent);
}
