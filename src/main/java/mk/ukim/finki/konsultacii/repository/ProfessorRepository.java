package mk.ukim.finki.konsultacii.repository;

import mk.ukim.finki.konsultacii.model.Professor;


public interface ProfessorRepository extends JpaSpecificationRepository<Professor, String> {

}
