package mk.ukim.finki.konsultacii.repository;

import mk.ukim.finki.konsultacii.model.Student;

import java.util.Optional;


public interface StudentRepository extends JpaSpecificationRepository<Student, String> {

    Optional<Student> findByEmail(String email);
}
